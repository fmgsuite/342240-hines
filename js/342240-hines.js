$(document).ready(function() {

    //// MOVE HEADER WRAPPERS INSIDE CORRESPONDING HERO IMAGE <A> TAGS
    $('#hero1_wrapper').prependTo('#hero1');
    $('#hero2_wrapper').prependTo('#hero2');
    $('#hero3_wrapper').prependTo('#hero3');
    $('#hero4_wrapper').prependTo('#hero4');
    
    $('header .header-social a').attr('target','_blank');
    
    $('nav#main ul ul li a:contains("Login")').attr('target','_blank');
    $('nav#main ul li a:contains("Privacy Policy")').attr('target','_blank');

    //// FOOTER EDITS - ADD LOWER BAR AND MOVE COPYRIGHT AND SOCIAL MEDIA ICONS TO NEW BAR
    // social media in footer, add classes to a tag
    var icon_facebook = $('<i>').addClass('fa fa-facebook');
    var icon_twitter = $('<i>').addClass('fa fa-twitter');
    var icon_linkedin = $('<i>').addClass('fa fa-linkedin');
    $('footer .socialMedia .facebook').parent().append(icon_facebook);
    $('footer .socialMedia .twitter').parent().append(icon_twitter);
    $('footer .socialMedia .linkedIn').parent().append(icon_linkedin);
    $('footer .socialMedia .fa-facebook').parent().addClass('facebook');
    $('footer .socialMedia .fa-twitter').parent().addClass('twitter');
    $('footer .socialMedia .fa-linkedin').parent().addClass('linkedIn');

    //// INSERT SHORT HR AFTER HERO H2
    $('<hr class="short">').insertAfter('#hero h2');

    //// INSERT SHORT HR AFTER SUBPAGE H1
    $('<hr class="short">').insertAfter($('.subpage #content article h1').first());

    //// INDEX SIDEBAR WIDGETS
    $('#content .container aside section').addClass(function(index) {
        return "widget-" + index;
    });

    //// MAKE ENTIRE BOX CLICKABLE, FOR MOBILE
    $('.box-md').click(function() {
        window.location = $(this).find('a').attr('href');
        return false;
    });
    
    ///IMAGE CLASS FOR BOOTSTRAP
    $('.page-team #content article a img').addClass('img-responsive');
    $('.subpage #content article img').addClass('img-responsive');
    $('.subpage #content article iframe').wrap('<div class="embed-responsive embed-responsive-6by9"></div>');
    $('.subpage #content article iframe').addClass('embed-responsive embed-responsive-16by9');
    
    $('.homepage #content .container article .welcome .contactus').wrapInner('<span></span>');
    $('.homepage #content .container article .two .box-sm a').wrapInner('<span></span>');
    
    $('#content .container aside .newsletterSignUp .wrapper h2').html('Sign Up for Our Monthly eNewsletter');
    $('#content .container aside .sidebarWidget:contains("Quarterly")').addClass('quarterlyNews');

});
